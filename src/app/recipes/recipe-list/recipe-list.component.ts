import { Recipe } from '../recipe.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe [] = [
    new Recipe('A Test Recipe', 'This is a simple test', 'https://images.unsplash.com/photo-1495546968767-f0573cca821e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f6dc137052dfd093fdb2f7d1c2c96bfa&auto=format&fit=crop&w=2089&q=80')
  ];

  constructor() { }

  ngOnInit() {
  }

}
